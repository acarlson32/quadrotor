
#define EXTERN extern

#include "../include/quadcopter_main.h"

#define TOGGLE_THRES 1500

////////////////////////////////////////////////////////////////////////


// Watchdog response function that commands the quadcopter base control values.
// Intent: Stay motionless until communication restored.
void watchdog_idle(data_t* data, int16_t* channels_ptr)
{
    printf(">>> WATCHDOG IN EFFECT.\n");
    channels_ptr[0] = data->thrust_pwm_base;
    channels_ptr[1] = data->roll_pwm_base;
    channels_ptr[2] = data->pitch_pwm_base;
    channels_ptr[3] = data->yaw_pwm_base;
}

// Returns true if watchdog takeover from controller dropout is required
int watchdog_controller(data_t* data)
{
    // If we're not connected, then we need to use the watchdog.
    int need_watchdog = !data->state->connected;

    if (need_watchdog)
        printf(">>> Suggesting watchdog for CONTROLLER DROPOUT.\n");
    return !data->state->connected;
}

// Returns true if watchdog takeover from optitrack is required
int watchdog_optitrack(data_t* data)
{
    int is_dropout = 1;
    int len = 10;

    pthread_mutex_lock(&data->mutex);
    for (int i = 1; i < len; i++)
    {
        is_dropout = is_dropout && (data->optitrack_buffer[i - 1].x == data->optitrack_buffer[i].x);
        is_dropout = is_dropout && (data->optitrack_buffer[i - 1].y == data->optitrack_buffer[i].y);
        is_dropout = is_dropout && (data->optitrack_buffer[i - 1].z == data->optitrack_buffer[i].z);
    }
    pthread_mutex_unlock(&data->mutex);

    if (is_dropout)
        printf(">>> Suggesting watchdog for OPTITRACK DROPOUT.\n");
    return is_dropout;
}


void auto_mode_handler(data_t* data, const channels_t* msg)
{
    double toggle_rdout = msg->channels[7];
    pthread_mutex_lock(&data->mutex);
    int auto_on = data->state->auto_on;
    pthread_mutex_unlock(&data->mutex);
    if (toggle_rdout < TOGGLE_THRES && auto_on == 1)
    {
        printf("SWITCHING TO TELEOP\n");
        pthread_mutex_lock(&data->mutex);
        auto_on = 0;
        pthread_mutex_unlock(&data->mutex);
    }
    else if (toggle_rdout > TOGGLE_THRES && auto_on == 0)
    {
        printf("SWITCHING TO AUTONOMOUS\n");
        pthread_mutex_lock(&data->mutex);
        data->state->auto_on = 1;
        data->thrust_pwm_base = msg->channels[0];
        data->roll_pwm_base = msg->channels[1];
        data->pitch_pwm_base = msg->channels[2];
        data->yaw_pwm_base = msg->channels[3];
        pthread_mutex_unlock(&data->mutex);
        printf("PWM Base Values Updated To:\n\t%i thrust, %i roll, %i pitch, and %i yaw.\n",
               data->thrust_pwm_base, data->roll_pwm_base,
               data->pitch_pwm_base, data->yaw_pwm_base);
    }
}

void channels_handler(const lcm_recv_buf_t *rbuf, const char *channel,
                      const channels_t *msg, void *user)
{
    data_t *data = (data_t*)user;
    // create a copy of the received message
    channels_t new_msg;
    new_msg.utime = msg->utime;
    new_msg.num_channels = msg->num_channels;
    new_msg.channels = (int16_t*) malloc(msg->num_channels*sizeof(int16_t));
    for(int i = 0; i < msg->num_channels; i++)
    {
        new_msg.channels[i] = msg->channels[i];
    }

    // Copy state to local state struct to minimize mutex lock time
    state_t localstate;
    pthread_mutex_lock(&state_mutex);
    memcpy(&localstate, data->state, sizeof(state_t));
    pthread_mutex_unlock(&state_mutex);

    auto_mode_handler(data, msg);

    // Decide whether or not to edit the motor message prior to sending it
    // set_points[] array is specific to geofencing.  You need to add code
    // to compute them for our FlightLab application!!!
    double set_points[8] = {0};
    int16_t pid_cmds[4] = {0};
    if (data->waypoint_length > 0)
    {
        auto_control(data, set_points, pid_cmds);
    }
    else
    {
        auto_control_clear();
        pid_cmds[0] = data->thrust_pwm_base;
        pid_cmds[1] = data->roll_pwm_base;
        pid_cmds[2] = data->pitch_pwm_base;
        pid_cmds[3] = data->yaw_pwm_base;
    }

    if (watchdog_optitrack(data) || watchdog_controller(data))
    {
        watchdog_idle(data, new_msg.channels);
    }
    else
    {
        pthread_mutex_lock(&data->mutex);
        int auto_on = data->state->auto_on;
        pthread_mutex_unlock(&data->mutex);
        if (auto_on == 1)
        {
            printf("AUTO MODE\n");
            if (data->motors != IDLE)
            {
                motor_power(data, new_msg.channels);
            }
            else
            {
                for (int i = 0; i < 4; i++)
                {
                    new_msg.channels[i] = pid_cmds[i];
                }
            }
        }
        else
        {
            printf("TELEOP MODE\n");
        }
        // Else pass through the controller's signals (implicit)
    }

    // send lcm message to motors
    new_msg.channels[7] = 1180; // "Failsafe" preventation per Ella Atkins
    channels_t_publish(data->lcm, "CHANNELS_1_TX", &new_msg);
    pthread_mutex_lock(&data->mutex);
    printf("Auto On: %i\n", data->state->auto_on);
    pthread_mutex_unlock(&data->mutex);

#if 1
    pthread_mutex_lock(&data->mutex);
    printf("Waypoint Index: %i\n", data->waypoint_index);
    pthread_mutex_unlock(&data->mutex);
    printf("Commanding: (Thrust %d), (Roll %d), (Pitch %d), (Yaw %d)\n",
          new_msg.channels[0],
          new_msg.channels[1],
          new_msg.channels[2],
          new_msg.channels[3]);
    printf("Controlling: ");
    if (pid_cmds[0] > data->thrust_pwm_base)
        printf("(Up %i)  ", pid_cmds[0]);
    else
        printf("(Down %i)  ", pid_cmds[0]);
    if (pid_cmds[1] > data->roll_pwm_base)
        printf("(Left %i)  ", pid_cmds[1]);
    else
        printf("(Right %i)  ", pid_cmds[1]);
    if (pid_cmds[2] > data->pitch_pwm_base)
        printf("(Forward %i)  ", pid_cmds[2]);
    else
        printf("(Backward %i)  ", pid_cmds[2]);
    printf("\n");
    printf("Pose: %f %f %f\n",
           data->state->pose[0],
           data->state->pose[1],
           data->state->pose[2]);
    printf("Going To: %f %f %f\n",
           set_points[0],
           set_points[1],
           set_points[2]);
#endif
    // Save received (msg) and modified (new_msg) command data to file.
    // NOTE:  Customize as needed (set_points[] is for geofencing)
    fprintf(block_txt,"%ld,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%f,%f,%f,%f,%f,%f\n",
            (long int) msg->utime,msg->channels[0],msg->channels[1],msg->channels[2],msg->channels[3], msg->channels[7],
            new_msg.channels[0],new_msg.channels[1],new_msg.channels[2],new_msg.channels[3],new_msg.channels[7],
            pid_cmds[0], pid_cmds[1], pid_cmds[2], pid_cmds[3],
            set_points[0], set_points[1], set_points[2],
            data->state->pose[0], data->state->pose[1], data->state->pose[2]);
    fflush(block_txt);
}
