#define EXTERN extern

#include "../include/quadcopter_main.h"
#define STEP 0.1

void print_waypoints(data_t* data)
{
    printf("Waypoint List: \n");
    for (int i = 0; i < data->waypoint_length; i++)
    {
        printf("\t{%f, %f, %f}\n",
               data->waypoint_list[i].x,
               data->waypoint_list[i].y,
               data->waypoint_list[i].z);
    }
    printf("\n");
}


void handle_inputs(data_t* data, char* msg, int msg_len)
{
    // Check if new message
    if (msg_len == 0)
        return;

    if (msg_len == 1)
    {
        if (msg[0] == 'h')
        {
            printf("[HOLDING CURRENT POSE] \n");
            data->waypoint_index = 0;
            data->waypoint_length = 0; // clear waypoint list
            data->waypoint_list[data->waypoint_length].x = data->state->pose[0];
            data->waypoint_list[data->waypoint_length].y = data->state->pose[1];
            data->waypoint_list[data->waypoint_length].z = data->state->pose[2];
            data->waypoint_length++;
        }
        if (msg[0] == 'w')
        {
            printf("[ADDING WAYPOINT]\n");
            data->waypoint_index = 0;
            data->waypoint_list[data->waypoint_length].x = data->state->pose[0];
            data->waypoint_list[data->waypoint_length].y = data->state->pose[1];
            data->waypoint_list[data->waypoint_length].z = data->state->pose[2];
            data->waypoint_length++;
            print_waypoints(data);
        }
        if (msg[0] == 's')
        {
            printf("[FORCING STEP INPUT] (implement)\n");
        }
        if (msg[0] == 'p')
        {
            printf("[SETTING PIPE LOCATION]\n");
            double x = data->state->pose[0];
            double y = data->state->pose[1];
            double z = data->state->pose[2];

            data->waypoint_index = 0;
            data->waypoint_length = 0; // clear waypoint list

            // Add in hover point above pipe
            data->waypoint_list[data->waypoint_length].x = x;
            data->waypoint_list[data->waypoint_length].y = y;
            data->waypoint_list[data->waypoint_length].z = z - 1; // Stop meters above the pipe
            data->waypoint_length++;

            // Add in the final pipe waypoint
            data->waypoint_list[data->waypoint_length].x = x;
            data->waypoint_list[data->waypoint_length].y = y;
            data->waypoint_list[data->waypoint_length].z = z;
            data->waypoint_length++;

            print_waypoints(data);
        }
        if (msg[0] == 't')
        {
            printf("[TAKING OFF]\n");
            if (fabs(data->state->orientation[0]) < 10.0)
            {
                printf("[TAKEOFF] PROCEEDING - sufficiently horizontal for takeoff sequence.\n");
                data->waypoint_index = 0;
                data->waypoint_length = 0; // clear waypoint list
                data->waypoint_list[data->waypoint_length].x = data->state->pose[0];
                data->waypoint_list[data->waypoint_length].y = data->state->pose[1];
                data->waypoint_list[data->waypoint_length].z = data->state->pose[2] - 1;
                data->waypoint_length++;
                data->motors = POWER;
                print_waypoints(data);
            }
            else
            {
                printf("[TAKEOFF] ABORTING - too extreme of a takeoff angle. Decrease roll.\n");
            }
        }
        if (msg[0] == 'l')
        {
            printf("[INITIATING LANDING SEQUENCE]\n");
            double x = data->state->pose[0];
            double y = data->state->pose[1];
            double z = data->state->pose[2];

            data->waypoint_index = 0;
            data->waypoint_length = 0; // clear waypoint list

            // Add in the failsafe return location
            data->waypoint_list[data->waypoint_length].x = x;
            data->waypoint_list[data->waypoint_length].y = y;
            data->waypoint_list[data->waypoint_length].z = z;
            data->waypoint_length++;
            data->waypoint_index++;

            // Land on the pipe
            data->waypoint_list[data->waypoint_length].x = x;
            data->waypoint_list[data->waypoint_length].y = y;
            data->waypoint_list[data->waypoint_length].z = z + 1;
            data->waypoint_length++;

            print_waypoints(data);
        }
        if (msg[0] == 'm')
        {
            printf("[POWER CYCLING MOTORS]\n");
            data->waypoint_length = 0; // clear waypoint list
            data->waypoint_index = 0;
            data->motors = POWER;
        }
        if (msg[0] == '2')
        {	// STEP back
            data->waypoint_length=0;
            data->waypoint_list[data->waypoint_length].x = data->state->pose[0]-STEP;
            data->waypoint_list[data->waypoint_length].y = data->state->pose[1];
            data->waypoint_list[data->waypoint_length].z = data->state->pose[2];
            data->waypoint_length++;
        }
        if (msg[0] == '8')
        {	// STEP forward
            data->waypoint_length=0;
            data->waypoint_list[data->waypoint_length].x = data->state->pose[0]+STEP;
            data->waypoint_list[data->waypoint_length].y = data->state->pose[1];
            data->waypoint_list[data->waypoint_length].z = data->state->pose[2];
            data->waypoint_length++;
        }
        if (msg[0] == '4')
        {	//STEP left
            data->waypoint_length=0;
            data->waypoint_list[data->waypoint_length].x = data->state->pose[0];
            data->waypoint_list[data->waypoint_length].y = data->state->pose[1]-STEP;
            data->waypoint_list[data->waypoint_length].z = data->state->pose[2];
            data->waypoint_length++;
        }
        if (msg[0] == '6')
        {	// STEP right
            data->waypoint_length=0;
            data->waypoint_list[data->waypoint_length].x = data->state->pose[0];
            data->waypoint_list[data->waypoint_length].y = data->state->pose[1]+STEP;
            data->waypoint_list[data->waypoint_length].z = data->state->pose[2];
            data->waypoint_length++;
        }
        if (msg[0] == '-')
        {	// STEP up
            data->waypoint_length=0;
            data->waypoint_list[data->waypoint_length].x = data->state->pose[0];
            data->waypoint_list[data->waypoint_length].y = data->state->pose[1];
            data->waypoint_list[data->waypoint_length].z = data->state->pose[2]+STEP;
            data->waypoint_length++;
        }
        if (msg[0] == '+')
        {	// STEP down
            data->waypoint_length=0;
            data->waypoint_list[data->waypoint_length].x = data->state->pose[0];
            data->waypoint_list[data->waypoint_length].y = data->state->pose[1];
            data->waypoint_list[data->waypoint_length].z = data->state->pose[2]-STEP;
            data->waypoint_length++;
        }
    }

    if (msg_len > 1)
    {
        if (msg[0] == 'w' && msg[1] == 'c')
        {
            printf("[CLEARING WAYPOINT LIST]\n", &msg[3]);
            data->waypoint_index = 0;
            data->waypoint_length = 0; // clear waypoint list
            print_waypoints(data);
        }
        if (msg[0] == 's' && msg[1] == 't' && msg[2] == ' ')
        {
            printf("[FORCING STEP INPUT TO %s] (implement)\n", &msg[3]);
        }
        if (msg[0] == 'k' && msg[2] == ' ')
        {
            if (msg[1] == 'p')
            {
                printf("[RECONFIGURING KP TO %s] (implement)\n", &msg[3]);
            }
            if (msg[1] == 'i')
            {
                printf("[RECONFIGURING KI TO %s] (implement)\n", &msg[3]);
            }
            if (msg[1] == 'd')
            {
                printf("[RECONFIGURING KD TO %s] (implement)\n", &msg[3]);
            }
        }
    }
}


void *ground_control(void *user)
{
    data_t *data = (data_t*)user;
    sleep(1);
    printf("\n\n♫  Ground Control to Major Tom ♫  \n");

    char input[80];
    int count;
    while(1)
    {
        printf("Awaiting a command:\n    ");
        count = 0;
        while (scanf("%c", &input[count]) == 1)
        {
            if (count >= 75)
                break;
            if (input[count] == '\n')
                break;
            count++;
        }
        input[count] = '\0';

        pthread_mutex_lock(&data->mutex);
        handle_inputs(data, input, count);
        pthread_mutex_unlock(&data->mutex);
    }
}
