//
// Top-level program for quadcopter
//
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <inttypes.h>
#include <unistd.h>
#include <pthread.h>

#define EXTERN   // main() program definition

#include "../include/quadcopter_main.h"

/*  ########################################
###    QUADCOPTER MAIN FUNCTION      ###
########################################

Code has several threads.
Comment the ones you do not need.
List below show the threads you NEED to run it.

### FOR THE QUADROTOR TO FLY ###
-> lcm_thread_loop
-> processing_loop
-> run_motion_capture
-> run_imu

### FOR TESTING MOTION CAPTURE ONLY ###
-> run_motion_capture
*/

void initialize_filenames();

int main()
{
    time_t now = time(NULL);
    strftime(LOG_DATE, sizeof(LOG_DATE)-1, "%Y-%m-%d--%H-%M", localtime(&now));
    printf("\n\n    /-------------------------------------------\\"
           "\n    |-------------------------------------------|"
           "\n    |       ROB 550 - Team 2 - Flight Lab       |"
           "\n    |Alexa Carlson, Kurt Lundeen, Sean Messenger|"
           "\n    |  Source Compiled On %s %s  |"
           "\n    |        Log Date: %s        |"
           "\n    |-------------------------------------------|"
           "\n    \\-------------------------------------------/"
           "\n\n\n", __DATE__, __TIME__, LOG_DATE);

    initialize_filenames();

    // Open file to capture command RX, TX, and pertinent guidance data
    block_txt = fopen(FILENAME_BLOCK, "a");

    data_t *data = (data_t*)calloc(1, sizeof(data_t));
    data->motors = IDLE;

    // Initialize the data mutexes
    pthread_mutex_init(&imu_mutex, NULL);
    pthread_mutex_init(&mcap_mutex, NULL);
    pthread_mutex_init(&state_mutex, NULL);
    pthread_mutex_init(&gcontrol_mutex, NULL);

    // Start the threads
    pthread_t lcm_thread;
    pthread_t processing_thread;
    pthread_t imu_thread;
    pthread_t motion_capture_thread;
    pthread_t ground_control_thread;

    pthread_create(&processing_thread, NULL, processing_loop, data);
    usleep(1000);
    pthread_create(&lcm_thread, NULL, lcm_thread_loop, data);
    pthread_create(&imu_thread, NULL, run_imu, NULL);
    pthread_create(&motion_capture_thread, NULL, run_motion_capture, NULL);
    pthread_create(&ground_control_thread, NULL, ground_control, data);

    // Join threads upon completetion
    pthread_join(lcm_thread, NULL);
    pthread_join(processing_thread, NULL);
    pthread_join(imu_thread, NULL);
    pthread_join(motion_capture_thread, NULL);
    pthread_join(ground_control_thread, NULL);

    fclose(block_txt);
    return 0;
}

void initialize_filenames()
{
    FILENAME_BLOCK[0] = '\0';
    filename_generate("block", FILENAME_BLOCK);
    FILENAME_MCAP[0] = '\0';
    filename_generate("mcap", FILENAME_MCAP);
    FILENAME_IMU[0] = '\0';
    filename_generate("imu", FILENAME_IMU);
    FILENAME_CONTROLLER[0] = '\0';
    filename_generate("controller", FILENAME_CONTROLLER);
}

/*
   LCM processing (top-level loop) -- Only use to talk with BLOCKS
   */
void *lcm_thread_loop(void *user)
{
    data_t *data = (data_t*)user;
    lcm_t* lcm = lcm_create(NULL);
    pthread_mutex_lock(&data->mutex);
    data->lcm = lcm;
    pthread_mutex_unlock(&data->mutex);
    channels_t_subscribe(lcm, "CHANNELS_1_RX", channels_handler, data);
    while(1)
        lcm_handle(lcm);
    lcm_destroy(lcm);
    return 0;
}

