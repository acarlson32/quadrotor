// Processing loop (contains geofence code as an example only)
// processing_loop():  Top-level thread function
// update_set_points():  Geofencing reference points (may be useful as a guide)
// processing_loop_initialize():  Set up data processing / geofence variables
// read_config:  Read configuration parameters from a file (customize please)
//
#define EXTERN extern
#define PROC_FREQ 100 // in Hz

#define WATCHDOG_THRES 1000000

#include "../include/quadcopter_main.h"
void read_config(char* filename, data_t* data);
int processing_loop_initialize(data_t* data);
void initialize_log();
void log_controller(double time, double* pose, double* target, int16_t* signals);
/*
 * State estimation and guidance thread
 */

void *processing_loop(void *user)
{
    data_t *data = (data_t*)user;
    long watchdog_last_time = utime_now();

    int hz = PROC_FREQ;
    processing_loop_initialize(data);

    // Local copies
    struct motion_capture_obs *mcobs[2];  // [0] = new, [1] = old

    while (1)
    {
        pthread_mutex_lock(&mcap_mutex);
        mcobs[0] = mcap_obs;

        if(mcap_obs[1].time > 0)
        {
            watchdog_last_time = utime_now();

            pthread_mutex_lock(&data->mutex);
            data->state->connected = 1;
            int index = data->optitrack_index % 10;
            data->optitrack_buffer[index].x = mcobs[0]->pose[0];
            data->optitrack_buffer[index].y = mcobs[0]->pose[1];
            data->optitrack_buffer[index].z = mcobs[0]->pose[2];
            data->optitrack_index++;
            pthread_mutex_unlock(&data->mutex);

            mcobs[1] = mcap_obs + 1;
        }
        else
        {
            if (utime_now() - watchdog_last_time > WATCHDOG_THRES)
            {
                pthread_mutex_lock(&data->mutex);
                data->state->connected = 0;
                pthread_mutex_unlock(&data->mutex);
            }
            mcobs[1] = mcap_obs;
        }
        pthread_mutex_unlock(&mcap_mutex);

        pthread_mutex_lock(&data->mutex);
        data->state->time = mcobs[0]->time;
        data->state->pose[0] = mcobs[0]->pose[0]; // x position
        data->state->pose[1] = mcobs[0]->pose[1]; // y position
        data->state->pose[2] = mcobs[0]->pose[2]; // z position / altitude
        data->state->pose[3] = mcobs[0]->pose[5]; // yaw angle
        data->state->pose[4] = 0;
        data->state->pose[5] = 0;
        data->state->pose[6] = 0;
        data->state->pose[7] = 0;
        data->state->orientation[0] = mcobs[0]->pose[3] * 180.0 / 3.14159;
        data->state->orientation[1] = mcobs[0]->pose[4] * 180.0 / 3.14159;
        data->state->orientation[2] = mcobs[0]->pose[5] * 180.0 / 3.14159;
        pthread_mutex_unlock(&data->mutex);

        // Estimate velocities from first-order differentiation
        double mc_time_step = mcobs[0]->time - mcobs[1]->time;
        if(mc_time_step > 1.0E-7 && mc_time_step < 1)
        {
            pthread_mutex_lock(&data->mutex);
            data->state->pose[4] = (mcobs[0]->pose[0]-mcobs[1]->pose[0])/mc_time_step;
            data->state->pose[5] = (mcobs[0]->pose[1]-mcobs[1]->pose[1])/mc_time_step;
            data->state->pose[6] = (mcobs[0]->pose[2]-mcobs[1]->pose[2])/mc_time_step;
            data->state->pose[7] = (mcobs[0]->pose[5]-mcobs[1]->pose[5])/mc_time_step;
            pthread_mutex_unlock(&data->mutex);
        }

        double target[4] = {5, 5, 5, 5};
        int16_t planned_signals[4] = {0};
        auto_control(data, target, planned_signals);
        log_controller(data->state->time, data->state->pose, target, planned_signals);

        usleep(1000000/hz);

    } // end while processing_loop()

    return 0;
}


/**
 * processing_loop_initialize()
 */
int processing_loop_initialize(data_t* data)
{
    pthread_mutex_lock(&data->mutex);
    // Initialize state struct
    data->state = (state_t*) calloc(1,sizeof(*state));
    data->state->time = ((double)utime_now())/1000000;
    data->state->connected = 1;
    memset(data->state->pose, 0, sizeof(state->pose));
    pthread_mutex_unlock(&data->mutex);

    // Read configuration file
    char config_filename[] = "config.txt";
    read_config(config_filename, data);

    initialize_log();

    mcap_obs[0].time = data->state->time;
    mcap_obs[1].time = -1.0;  // Signal that this isn't set yet

    // Initialize Altitude Velocity Data Structures
    memset(diff_z, 0, sizeof(diff_z));
    memset(diff_z_med, 0, sizeof(diff_z_med));

    // Fence variables
    printf("Initializing variables...\n");
    pthread_mutex_lock(&data->mutex);
    data->state->auto_on = 0;
    pthread_mutex_unlock(&data->mutex);

    // Initialize IMU data
    //if(imu_mode == 'u' || imu_mode == 'r'){
    //  imu_initialize();
    //}

    return 0;
}

void read_config(char* filename, data_t* data)
{
    // holder string
    char str[1000];

    // open configuration file
    FILE* conf = fopen(filename,"r");

    // read in the quadrotor initial position
    pthread_mutex_lock(&data->mutex);
    fscanf(conf,"%s %lf %lf %lf %lf",str,&data->quad_start[0],&data->quad_start[1], &data->quad_start[2], &data->quad_start[3]);
    printf("Configuring START as:  %3.3f / %f / %f / %f\n", data->quad_start[0], data->quad_start[1], data->quad_start[2], data->quad_start[3]);
    fscanf(conf,"%s %lf %lf %lf %lf",str,&data->pipe[0],&data->pipe[1], &data->pipe[2], &data->pipe[3]);
    printf("Configuring PIPE as:   %f / %f / %f / %f\n", data->pipe[0], data->pipe[1], data->pipe[2], data->pipe[3]);

    fscanf(conf,"%s %lf %lf %lf",str, &data->kp[0],&data->kp[1], &data->kp[2]);
    printf("Configuring KP as:     %f / %f / %f\n", data->kp[0], data->kp[1], data->kp[2]);
    fscanf(conf,"%s %lf %lf %lf",str, &data->ki[0],&data->ki[1], &data->ki[2]);
    printf("Configuring KD as:     %f / %f / %f\n", data->ki[0], data->ki[1], data->ki[2]);
    fscanf(conf,"%s %lf %lf %lf",str, &data->kd[0],&data->kd[1], &data->kd[2]);
    printf("Configuring KI as:     %f / %f / %f\n", data->kd[0], data->kd[1], data->kd[2]);

    fscanf(conf,"%s %lf %lf %lf",str, &data->thres_integral, &data->delta_roll_pitch, &data->delta_thrust);
    printf("Configuring THRES as:  %f / %f / %f\n", data->thres_integral, data->delta_roll_pitch, data->delta_thrust);

    fscanf(conf,"%s %i %i %i",str, &data->roll_pwm_right, &data->roll_pwm_base, &data->roll_pwm_left);
    printf("Configuring ROLL as:   %i / %i / %i\n", data->roll_pwm_right, data->roll_pwm_base, data->roll_pwm_left);
    fscanf(conf,"%s %i %i %i",str, &data->pitch_pwm_backward, &data->pitch_pwm_base, &data->pitch_pwm_forward);
    printf("Configuring PITCH as:  %i / %i / %i\n", data->pitch_pwm_backward, data->pitch_pwm_base, data->pitch_pwm_forward);
    fscanf(conf,"%s %i %i %i",str, &data->thrust_pwm_down, &data->thrust_pwm_base, &data->thrust_pwm_up);
    printf("Configuring THRUST as: %i / %i / %i\n", data->thrust_pwm_down, data->thrust_pwm_base, data->thrust_pwm_up);
    fscanf(conf,"%s %i %i %i",str, &data->yaw_pwm_cw, &data->yaw_pwm_base, &data->yaw_pwm_ccw);
    printf("Configuring YAW as:    %i / %i / %i\n", data->yaw_pwm_cw, data->yaw_pwm_base, data->yaw_pwm_ccw);
    pthread_mutex_unlock(&data->mutex);

    fclose(conf);
}

void initialize_log()
{
    controller_txt = fopen(FILENAME_CONTROLLER, "a");
}

void log_controller(double time, double* pose, double* target, int16_t* signals)
{
    fprintf(controller_txt,"%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%i,%i,%i,%i\n",
            time,
            pose[0], pose[1], pose[2], pose[3],
            target[0], target[1], target[2], target[3],
            signals[0], signals[1], signals[2], signals[3]);
    fflush(controller_txt);
}


