//
// auto_control:  Function to generate autonomous control PWM outputs.
//
#define EXTERN extern
#include "../include/quadcopter_main.h"

#define ROLL_INDEX 0
#define PITCH_INDEX 1
#define ALT_INDEX 2
#define WAY_THRESH 0.20

// Motor power cycle defines
#define POWER_THRUST 1100
#define POWER_ROLL 1900
#define POWER_PITCH 1100
#define POWER_YAW 1100
#define MOTOR_TIME_THRES 0.5 // In seconds
#define VELO_THRESH 0.1 // meters per second
#define CAP_REG_THRESH 10

// define the gain coefficient arrays; will hold gains for roll, pitch , altitude
double integral_term[3] = {0};
double error_prev[3] = {0};

time_t time_motor_start = 0;
int motor_init_state = 0;

double pid_controller(data_t* data, double dist_error, int k_index);

void motor_power(data_t* data, int16_t* channels)
{
    if (data->motors == POWER)
    {
        printf("[POWER] Initiating power cycle...\n");
        time_motor_start = time(NULL);
        data->motors = INITIALIZING;
    }
    else if (data->motors == INITIALIZING)
    {
        printf("[POWER] Slowing down thrust...\n");
        channels[0] = POWER_THRUST;
        channels[1] = data->roll_pwm_base;
        channels[2] = data->pitch_pwm_base;
        channels[3] = data->yaw_pwm_base;
        if (time(NULL) - time_motor_start > MOTOR_TIME_THRES)
        {
            time_motor_start = time(NULL);
            data->motors = INITIALIZED;
        }
    }
    else if (data->motors = INITIALIZED)
    {
        printf("[POWER] Commanding motor extremes...\n");
        channels[0] = POWER_THRUST;
        channels[1] = POWER_ROLL;
        channels[2] = POWER_PITCH;
        channels[3] = POWER_YAW;
        if (time(NULL) - time_motor_start > MOTOR_TIME_THRES)
        {
            printf("[POWER] Motor power sequence COMPLETE.\n");
            time_motor_start = time(NULL);
            data->motors = IDLE;
        }
    }
}

void auto_control_clear()
{
    integral_term[0] = 0;
    integral_term[1] = 0;
    integral_term[2] = 0;
}

// Outer loop controller to generate PWM signals for the Naza-M autopilot
void auto_control(data_t* data, double *set_points, int16_t* channels_ptr)
{
    // Channels for you to set:
    // [0] = thrust
    // [1] = roll
    // [2] = pitch
    // [3] = yaw


    //printf("on waypoint %d, which is %f, %f, %f \n", i, temp_waypoint.x, temp_waypoint.y, temp_waypoint.z);
    pthread_mutex_lock(&data->mutex);
    double* pose = data->state->pose;
    point_t temp_waypoint = data->waypoint_list[data->waypoint_index];
    if (data->state->auto_on)
    {
        int within_x = (fabs(pose[0] - temp_waypoint.x) < WAY_THRESH);
        int within_y = (fabs(pose[1] - temp_waypoint.y) < WAY_THRESH);
        int within_z = (fabs(pose[2] - temp_waypoint.z) < WAY_THRESH);
        if (within_x && within_y && within_z)
        {
            if (pose[4] < VELO_THRESH && pose[6] <VELO_THRESH && pose[7] < VELO_THRESH)
            {
                if (data->waypoint_index < data->waypoint_length-1)
                {
                    data->waypoint_index++;
                    printf("moving to waypoint %d\n",data->waypoint_index);
                }
                else if(data->waypoint_index == data->waypoint_length-1)
                {
                    // Landed! Initiate shutdown sequence
#if 0
                    for (int i = 0; i < 20; i++)
                        printf("LANDED LANDED LANDED\n");
                    data->waypoint_length = 0;
                    data->waypoint_index = 0;
                    data->motors = POWER;
#endif

                    if (fabs(data->state->orientation[0])>CAP_REG_THRESH)
                    {
                        printf("ANGLE EXCEEDS MAX ALLOWED ROLL: aborting landing and returning to previous waypoint\n");
                        data->waypoint_index--;
                    }
                    if( fabs(data->state->orientation[1])> CAP_REG_THRESH)
                    {
                        printf("ANGLE EXCEEDS MAX ALLOWED PITCH: aborting landing and returning to previous waypoint\n");
                        data->waypoint_index--;
                    }
                }
            }
        }
    }

    point_t dest_waypoint = data->waypoint_list[data->waypoint_index];
    set_points[0] = dest_waypoint.x;
    set_points[1] = dest_waypoint.y;
    set_points[2] = dest_waypoint.z;

    double dx = set_points[0] - data->state->pose[0];
    double dy = set_points[1] - data->state->pose[1];
    double dz = set_points[2] - data->state->pose[2];

    double dist = sqrt(pow(dx, 2) + pow(dy, 2));
    double omega = atan2(-dy, dx);
    double theta = omega - data->state->pose[3]; //not sure whether deg or rad, but assume rad
    pthread_mutex_unlock(&data->mutex);

    double dist_rob_x = dist * cos(theta);
    double dist_rob_y = dist * sin(theta);

    double roll_setcommand = pid_controller(data, dist_rob_y, ROLL_INDEX);
    double pitch_setcommand = pid_controller(data, dist_rob_x, PITCH_INDEX);
    double altitude_setcommand = pid_controller(data, dz, ALT_INDEX);

    pthread_mutex_lock(&data->mutex);
    channels_ptr[0] = altitude_setcommand;
    channels_ptr[1] = roll_setcommand;
    channels_ptr[2] = pitch_setcommand;
    channels_ptr[3] = data->yaw_pwm_base;
    pthread_mutex_unlock(&data->mutex);
}


double pid_controller(data_t* data, double dist_error, int k_index)
{
    integral_term[k_index] += dist_error;

    pthread_mutex_lock(&data->mutex);
    double setcommand1 = data->kp[k_index] * dist_error;
    double setcommand2 = data->kd[k_index] * (dist_error - error_prev[k_index]);
    double setcommand3 = data->ki[k_index] * integral_term[k_index];
    double thres_integral = data->thres_integral;
    double delta_roll_pitch = data->delta_roll_pitch;
    double delta_thrust = data->delta_thrust;

    error_prev[k_index] = dist_error;
    // integral term clamping
    if (fabs(setcommand3) > thres_integral)
    {
        integral_term[k_index] = (setcommand3 > 0) ? thres_integral : -thres_integral;
        integral_term[k_index] /= data->ki[k_index];
    }
    pthread_mutex_unlock(&data->mutex);

    double setcommand = setcommand1 + setcommand2 + setcommand3;

    // smart clamping
    if (k_index == ALT_INDEX)
    {
        if (fabs(setcommand) > delta_thrust)
        {
            setcommand = (setcommand > 0) ? delta_thrust : -delta_thrust;
        }
    }
    else
    {
        if (fabs(setcommand) > delta_roll_pitch)
        {
            setcommand = (setcommand > 0) ? delta_roll_pitch : -delta_roll_pitch;
        }
    }

    // ensure setcommand is withing the min and max values of Naza
    if(k_index == ROLL_INDEX)
    { // we are dealing with roll
        setcommand += data->roll_pwm_base;
        if (setcommand > data->roll_pwm_left)
        {
            setcommand = data->roll_pwm_left;
        }
        else if (setcommand < data->roll_pwm_right)
        {
            setcommand = data->roll_pwm_right;
        }
    }
    else if (k_index == PITCH_INDEX)
    { // we are dealing with pitch
        setcommand += data->pitch_pwm_base;
        if (setcommand > data->pitch_pwm_forward)
        {
            setcommand = data->pitch_pwm_forward;
        }
        else if (setcommand < data->pitch_pwm_backward)
        {
            setcommand = data->pitch_pwm_backward;
        }
    }
    else if (k_index == ALT_INDEX)
    { // we are dealing with altitude
	setcommand *= -1; // Invert sign for correct direction
        setcommand += data->thrust_pwm_base;
        if (setcommand > data->thrust_pwm_up)
        {
            setcommand = data->thrust_pwm_up;
        }
        else if ( setcommand < data->thrust_pwm_down)
        {
            setcommand = data->thrust_pwm_down;
        }
    }

    return  setcommand;
}


