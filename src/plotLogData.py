# plotLogData.py
# This program plots the flight log data.
# ROB 550
# Lab 5
# Alexa Carlson (askc), Kurt Lundeen (klundeen), Sean Messenger (smess)
# December 2015

# Imports
import csv
from matplotlib.pyplot import *
#from numpy import *
import numpy as np
import matplotlib.gridspec as gridspec
from math import *
from mpl_toolkits.mplot3d import Axes3D

# Parameters
DIRECTORY = '../logs/Lab_2015-12-17/'
LOG_DATE = '2015-03-01'
LOG_TIME = '21-16'

# Functions
def LoadData(dataType):
    filename = '%s%s-%s--%s.csv' % (DIRECTORY, dataType, LOG_DATE, LOG_TIME)
    with open(filename,'r') as inputFile:
        reader = csv.reader(inputFile)
        return list(reader)

def ConvertMatrixToFloats(dataStrings,dims,dataType):
    dataFloats = np.zeros((dims[0],dims[1]))
    for i in range(dims[0]):
        for j in range(dims[1]):
            if j == 0:
                if dataType == 'block' or dataType == 'imu':
                    dataFloats[i][j] = (float(dataStrings[i][j]) \
                        - float(dataStrings[0][j])) / 10**6
                else:
                    dataFloats[i][j] = (float(dataStrings[i][j]) \
                        - float(dataStrings[0][j]))
            else:
                dataFloats[i][j] = float(dataStrings[i][j])
    return dataFloats

# Create data matrices
#blockDataStrings = LoadData('block')
#blockDims = (len(blockDataStrings),len(blockDataStrings[0]))
#blockData = ConvertMatrixToFloats(blockDataStrings,blockDims,'block')
cntrlDataStrings = LoadData('controller')
cntrlDims = (len(cntrlDataStrings),len(cntrlDataStrings[0]))
cntrlData = ConvertMatrixToFloats(cntrlDataStrings,cntrlDims,'controller')
imuDataStrings = LoadData('imu')
imuDims = (len(imuDataStrings),len(imuDataStrings[0]))
imuData = ConvertMatrixToFloats(imuDataStrings,imuDims,'imu')
imuRoll_deg = np.arcsin(imuData[:,5])*180/pi
imuPitch_deg = np.arcsin(imuData[:,4])*180/pi
mcapDataStrings = LoadData('mcap')
mcapDims = (len(mcapDataStrings),len(mcapDataStrings[0]))
mcapData = ConvertMatrixToFloats(mcapDataStrings,mcapDims,'mcap')
for i in range(len(mcapData)):
    if mcapData[i,4] < -pi/2:
        mcapData[i,4] = mcapData[i,4] + pi 
    elif mcapData[i,4] > pi/2:
        mcapData[i,4] = mcapData[i,4] - pi
mcapFreq = np.zeros(np.size(mcapData,0))
for i in range(len(mcapFreq)):
    if i == 0:
        mcapFreq[i] = 1/(mcapData[i+1][0] - mcapData[i][0])
    else:
        mcapFreq[i] = 1/(mcapData[i][0] - mcapData[i-1][0])
mcapFreqMean = np.mean(mcapFreq[:])
for i in range(len(mcapFreq)):
    if mcapFreq[i] > 1000:
        mcapFreq[i] = 1000
mcapMaxDelay = 1/min(mcapFreq[:])
mcapStaticData = np.zeros((len(mcapData),2))
staticCntCrit = 20
staticAngCrit = 1 * pi/180
j = 0
for i in range(staticCntCrit, len(mcapData)):
    if (max(mcapData[i-staticCntCrit:i,4]) - min(mcapData[i-staticCntCrit:i,4])) < staticAngCrit:
        mcapStaticData[j][0] = mcapData[i][2] + 0.75
        mcapStaticData[j][1] = mcapData[i][3] + 0.42
        j = j + 1
        
'''
# Plot block data
figure()
subplot(4,1,1)
title('Block Commands', fontsize = 18)
plot(blockData[:,0],blockData[:,1])
ylabel('Thrust [$\mu$s]', fontsize = 16)
axis((0,max(blockData[:,0]),1000,2000))
yticks(np.arange(1000,2000+250,250))
tick_params(labelbottom='off')
grid()
subplot(4,1,2)
plot(blockData[:,0],blockData[:,2])
ylabel('Roll [$\mu$s]', fontsize = 16)
axis((0,max(blockData[:,0]),1000,2000))
yticks(np.arange(1000,2000+250,250))
tick_params(labelbottom='off')
grid()
subplot(4,1,3)
plot(blockData[:,0],blockData[:,3])
ylabel('Pitch [$\mu$s]', fontsize = 16)
axis((0,max(blockData[:,0]),1000,2000))
yticks(np.arange(1000,2000+250,250))
grid()
tick_params(labelbottom='off')
subplot(4,1,4)
plot(blockData[:,0],blockData[:,4])
ylabel('Yaw [$\mu$s]', fontsize = 16)
axis((0,max(blockData[:,0]),1000,2000))
yticks(np.arange(1000,2000+250,250))
xlabel('Time (s)', fontsize = 16)
grid()
savefig('%sblock-%s---%s.png' % (DIRECTORY, LOG_DATE, LOG_TIME))
show()
'''

# Plot Opti-Track data
figure()
yInc = 0.5
subplot(6,1,1)
title('Opti-Track Data', fontsize = 18)
plot(mcapData[:,0],mcapData[:,1])
ylabel('X [m]', fontsize = 14)
yticks(np.arange((floor(min(mcapData[:,1])/yInc))*yInc, ceil(max(mcapData[:,1])/yInc)*yInc, yInc))
tick_params(labelbottom='off')
grid()
subplot(6,1,2)
plot(mcapData[:,0],mcapData[:,2])
ylabel(r'Y [m]', fontsize = 14)
yticks(np.arange((floor(min(mcapData[:,2])/yInc))*yInc, ceil(max(mcapData[:,2])/yInc)*yInc, yInc))
tick_params(labelbottom='off')
grid()
subplot(6,1,3)
plot(mcapData[:,0],mcapData[:,3])
ylabel('Z [m]', fontsize = 14)
yticks(np.arange((floor(min(mcapData[:,3])/yInc))*yInc, ceil(max(mcapData[:,3])/yInc)*yInc, yInc))
tick_params(labelbottom='off')
tick_params(labelbottom='off')
grid()
yInc = 2
subplot(6,1,4)
plot(mcapData[:,0],mcapData[:,4]*180/pi)
ylabel(r'Roll [$\degree$]', fontsize = 14)
yticks(np.arange((floor(min(mcapData[:,4]*180/pi)/yInc))*yInc, ceil(max(mcapData[:,4]*180/pi)/yInc)*yInc, yInc))
tick_params(labelbottom='off')
grid()
subplot(6,1,5)
plot(mcapData[:,0],mcapData[:,5]*180/pi)
ylabel(r'Pitch [$\degree$]', fontsize = 14)
yticks(np.arange((floor(min(mcapData[:,5]*180/pi)/yInc))*yInc, ceil(max(mcapData[:,5]*180/pi)/yInc)*yInc, yInc))
tick_params(labelbottom='off')
grid()
subplot(6,1,6)
plot(mcapData[:,0],mcapData[:,6]*180/pi)
ylabel(r'Yaw [$\degree$]', fontsize = 14)
yticks(np.arange((floor(min(mcapData[:,6]*180/pi)/yInc))*yInc, ceil(max(mcapData[:,6]*180/pi)/yInc)*yInc, yInc))
grid()
xlabel('Time [s]', fontsize = 14)
savefig('%smcap-%s---%s.png' % (DIRECTORY, LOG_DATE, LOG_TIME))
show()

# Plot Pitch and Roll Data from IMU and Optitrack
figure()
subplot(2,1,1)
title('IMU vs Optitrack', fontsize = 18)
plot(imuData[:,0],imuRoll_deg,'g', label='IMU')
plot(mcapData[:,0],mcapData[:,4]*180/pi, 'r', label='Opti')
ylabel(r'Roll [$\degree$]', fontsize = 14)
grid()
legend()
subplot(2,1,2)
plot(imuData[:,0],imuPitch_deg,'g', label='IMU')
plot(mcapData[:,0],mcapData[:,5]*180/pi, 'r', label='Opti')
ylabel(r'Pitch [$\degree$]', fontsize = 14)
xlabel('Time [s]', fontsize = 14)
grid()
legend()
savefig('%simu-opti-%s---%s.png' % (DIRECTORY, LOG_DATE, LOG_TIME))
show()

# Plot Optitrack Data Rates
figure
subplot(2,1,1)
title('Optitrack Data Rates in Lab', fontsize = 18)
#title('Optitrack Data Rates in Atrium', fontsize = 18)
ylabel('Dropouts [???]', fontsize = 14)
tick_params(labelbottom='off')
subplot(2,1,2)
plot(mcapData[:,0],mcapFreq[:],)
ylabel('Freq [Hz]', fontsize = 14)
#axis((0,max(mcapData[:,0]),0,1000))
xlabel('Time [s]', fontsize = 14)
figtext(0.71, 0.4, 'Avg Freq: %d Hz\nMax Delay: %.2f s' % (mcapFreqMean,mcapMaxDelay), backgroundcolor='w')
grid()
savefig('%sopti-freq-%s---%s.png' % (DIRECTORY, LOG_DATE, LOG_TIME))
show()

# Plot Path
fig = figure()
ax = Axes3D(fig)
title('Quadrotor Path', fontsize = 18)
ax.scatter(mcapData[:,1],-mcapData[:,2],-mcapData[:,3])
ax.set_xlabel('+X [m]', fontsize = 14)
ax.set_ylabel('-Y [m]', fontsize = 14)
ax.set_zlabel('-Z [m]', fontsize = 14)
ax.view_init(elev=20, azim=160)
savefig('%squad-path-%s---%s.png' % (DIRECTORY, LOG_DATE, LOG_TIME))
show()

# Plot Gripper Capture Region
fig = figure()
title('Gripper Capture Region', fontsize = 18)
axis('equal')
scatter(mcapStaticData[:,0], -mcapStaticData[:,1], c='b', linewidths=0)
scatter(-mcapStaticData[:,0], -mcapStaticData[:,1], c='b', linewidths=0)
scatter(0, 0, s=40, c='w', edgecolors='w')
angleFull = np.linspace(0, 2*pi, 100)
angleBot = np.linspace(pi/6, pi/2, 100)
angleTop = np.linspace(pi/12, pi/2.2, 100)
plot(0.2 * np.cos(angleBot), 0.2 * np.sin(angleBot), 'b--', linewidth=2)
plot(-0.2 * np.cos(angleBot), 0.2 * np.sin(angleBot), 'b--', linewidth=2)
plot(0.21 * np.cos(angleTop) - 0.032, 0.21 * np.sin(angleTop) + 0.052, 'b--', linewidth=2)
plot(-(0.21 * np.cos(angleTop) - 0.032), 0.21 * np.sin(angleTop) + 0.052, 'b--', linewidth=2)
plot(0.009 * np.cos(angleFull), 0.009 * np.sin(angleFull), 'k', linewidth=2)
plot(0.013 * np.cos(angleFull), 0.013 * np.sin(angleFull), 'k', linewidth=2)
grid()
xlabel('+Y [m]', fontsize = 14)
ylabel('-Z [m]', fontsize = 14)
text(0.04, 0.01, 'Pipe', fontsize = 14)
plot([0.015,0.033],[0.005,0.012],'k') 
savefig('%sgripper-capture-%s---%s.png' % (DIRECTORY, LOG_DATE, LOG_TIME))
show()

# Plot Gripper Errors
figure()
title('Gripper Errors', fontsize = 18)
xlabel('Displacement Error [in]', fontsize = 14)
ylabel('Roll Error [$\degree$]', fontsize = 14)
grid()
savefig('%sgripper-error-%s---%s.png' % (DIRECTORY, LOG_DATE, LOG_TIME))
show()
