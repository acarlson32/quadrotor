#ifndef ROB550_STRUCT
#define ROB550_STRUCT

// Shared data structures for ROB 550 Quadrotor project (not LCM)

typedef struct imu imu_t;
struct imu  // Only includes gyro and accel data for now
{
    int64_t utime;
    double gyro_x, gyro_y, gyro_z;
    double accel_x, accel_y, accel_z;
};

/*
 * state:
 * struct holds values related to current state estimate and
 * autonomous control (fence) activity
 */
typedef struct state state_t;
struct state{
    double time;

    // aircraft position (x,y,alt,yaw,xdot,ydot,altdot,yawdot)
    double pose[8];
    // (roll, pitch)
    double orientation[3];

    // Flag indicating whether the autonomous controller is activ or not
    // (1 = on; 0 = pass through pilot commands)
    int auto_on;

    // Watchdog connection status flag. 1 is connected, 0 is timed out.
    int connected;

    double destination[8];
};


/*
 * point:
 * struct holds
 */
typedef struct point point_t;
struct point
{
    double x, y, z;
};

typedef enum {IDLE, POWER, INITIALIZING, INITIALIZED} power_state;

/*
 * data:
 * struct holds data worth transmitting between threads
 */
typedef struct data data_t;
struct data
{
    pthread_mutex_t mutex;

    state_t* state;

    double quad_start[4];   // [x,y,z,theta] for initial position of the quadrotor
    double pipe[4];         // [x,y,z,theta] for the pipe's position

    // [roll, pitch, thrust] gains
    double kp[3];
    double ki[3];
    double kd[3];

    // PID and Clamp Thresholds
    double thres_integral;
    double delta_roll_pitch;
    double delta_thrust;

    // PWM Values
    int roll_pwm_right, roll_pwm_base, roll_pwm_left;
    int pitch_pwm_backward, pitch_pwm_base, pitch_pwm_forward;
    int thrust_pwm_down, thrust_pwm_base, thrust_pwm_up;
    int yaw_pwm_cw, yaw_pwm_base, yaw_pwm_ccw;

    // waypoint list definition
    point_t waypoint_list[500];
    int waypoint_length;
    int waypoint_index;

    // Watchdogs
    point_t optitrack_buffer[10];
    int optitrack_index;

    // State machines
    power_state motors;

    lcm_t* lcm;
};


#endif
