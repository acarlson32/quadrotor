#ifndef QUADCOPTER_GLOBALS
#define QUADCOPTER_GLOBALS

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <inttypes.h>
#include <unistd.h>
#include <pthread.h>

#ifdef __cplusplus
extern "C" {
#endif

//Use this in your code:  #define EXTERN extern

#include <lcm/lcm.h>
#include "../include/lcmtypes/lcmtypes_c/channels_t.h"
#include "../include/quadcopter_struct.h"
#include "../include/bbblib/bbb.h"

// Primary threads
#include "../include/run_imu.h"
#include "../include/run_motion_capture.h"
#include "../include/util.h"

/* Global Variables with mutexes for sharing */

EXTERN imu_t imudata;
EXTERN struct motion_capture_obs mcap_obs[2];
EXTERN state_t *state;

/* Mutexes */
EXTERN pthread_mutex_t imu_mutex;
EXTERN pthread_mutex_t mcap_mutex;
EXTERN pthread_mutex_t state_mutex;
EXTERN pthread_mutex_t gcontrol_mutex;

/* Global variables that are not used in multiple threads (no mutex use) */
EXTERN FILE *mcap_txt, *block_txt, *imu_txt, *controller_txt; // Output data files
EXTERN char FILENAME_MCAP[50], FILENAME_BLOCK[50], FILENAME_IMU[50], FILENAME_CONTROLLER[50];

EXTERN int imu_mode, mcap_mode;
EXTERN struct imu_data *imu;

// Top-level thread function declarations
void *lcm_thread_loop(void *);
void *processing_loop(void *);
void *run_imu(void *);
void *run_motion_capture(void *);
void *ground_control(void *);

/* Function declarations needed for multiple files */
void channels_handler(const lcm_recv_buf_t *rbuf, const char *channel,
		      const channels_t *msg, void *userdata);
void auto_control(data_t* data, double *set_points, int16_t *channels_ptr);
void auto_control_clear();
void motor_power(data_t* data, int16_t* channels);

//////////////////////////////////////////
///  Geofence stuff for testing

#define NUM_SAMPLES_MED_ALT 10
#define NUM_SAMPLES_AVG_ALT 20

EXTERN double diff_z[NUM_SAMPLES_MED_ALT], diff_z_med[NUM_SAMPLES_AVG_ALT];
EXTERN double ang_buf;
EXTERN double fence_penalty_length;

//////////////////////////////////////////
//////////////////////////////////////////
//////////////////////////////////////////
// Logging
EXTERN char LOG_DATE[50];
//////////////////////////////////////////
//////////////////////////////////////////
//////////////////////////////////////////


// max desired velocity for fence corrections
EXTERN float max_vel, max_step;

EXTERN double alt_low_fence, alt_high_fence;
EXTERN double x_pos_fence, x_neg_fence, y_pos_fence, y_neg_fence;
EXTERN double alt_prev;

//////////////////////////////////////////////////////////////////////////////
// Geofence function
int update_set_points(double* pose, double* set_points, int first_time);

#ifdef __cplusplus
}
#endif

#endif
