# Add your targets (i.e. the name of your output program) here
OBJS = quadcopter_main.o auto_control.o command_handler.o \
       processing_loop.o run_imu.o run_motion_capture.o util.o \
	   ground_control.o
IDIR = 	include
_DEPS = quadcopter_main.h quadcopter_struct.h run_imu.h run_motion_capture.h util.h

BINDIR = bin
OBJDIR = obj
SRCDIR = src

LCMTYPES = cfg_data_frequency_t.o cfg_uart_baud_t.o cfg_usb_serial_num_t.o channels_t.o \
        kill_t.o

BBBOBJS = bbb_init.o bbb_i2c.o bbb_uart.o bbb_gpio.o

CC = g++
CC2 = gcc
CFLAGS = -g -Wall `pkg-config --cflags lcm`
LDFLAGS = `pkg-config --libs lcm`

OBJS := $(addprefix obj/,$(OBJS))

DEPS := $(addprefix $(IDIR)/,$(_DEPS))
LCMTYPES := $(addprefix include/lcmtypes/lcmtypes_c/,$(LCMTYPES))
BBBOBJS := $(addprefix include/bbblib/,$(BBBOBJS))

.PHONY: all clean io include/lcmtypes include/bbblib

all: include/lcmtypes include/bbblib $(OBJS) $(BINDIR)/comms_driver $(BINDIR)/quadcopter_main

include/lcmtypes:
	@$(MAKE) -C include/lcmtypes

include/bbblib:
	@$(MAKE) -C include/bbblib

### Basic Make data

blocks/io/%.o: blocks/io/%.c
	$(CC2) $(CFLAGS) -c $^ -I. -o $@

blocks/%.o: blocks/%.c
	$(CC2) $(CFLAGS) -c $^ -I. -o $@

$(OBJDIR)/%.o: $(SRCDIR)/%.c
	mkdir -p $(OBJDIR)
	$(CC) $(CFLAGS) -c $^ -o $@


include/lcmtypes/%.o: include/lcmtypes/%.c
	$(CC) $(CFLAGS) -c $^ -o $@


### Specific programs to build

$(BINDIR)/comms_driver: blocks/comms_driver.o blocks/io/comms.o blocks/io/serial.o blocks/io/circular.o
	mkdir -p $(BINDIR)
	$(CC2) -o $@ $^ -Iio $(LDFLAGS) $(LCMTYPES)

$(BINDIR)/quadcopter_main: $(OBJS)
	mkdir -p logs
	mkdir -p $(BINDIR)
	$(CC) -o $@ $^ $(LDFLAGS) $(LCMTYPES) $(BBBOBJS) -lm -lpthread

# Clean target
clean:
	@$(MAKE) -C include/lcmtypes clean
	@$(MAKE) -C include/bbblib clean
	rm -f *~ *.o src/*~ src/*.o include/*~ blocks/io/*.o blocks/*.o
	rm -rf obj bin
